<?php 



/**
 *
 * Plugin Name:       Taxdoo Gutenberg Blocks Plugin
 * Plugin URI:        https://taxdoo.com/de
 * Description:       Selfmade Plugin to provide a set of custom Gutenberg Blocks
 * Version:           1.0.0
 * Author:            Joshua Kresse
 * Author URI:        https://taxdoo.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       txd-gbb-plugin
 */


require 'src/Plugin.php';

use TxdWp\Plugin;

$plugin = new Plugin();

?>