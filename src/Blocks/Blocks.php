<?php 

namespace TxdWp\Blocks;
use TxdWP\Plugin;

class Blocks {

    public function taxdoo_toc_block_init(){

        

        $plugin = new Plugin();


        wp_register_script(
            'taxdoo_toc',
            plugins_url("taxdoo_toc/taxdoo_toc.js", __FILE__),
            ['wp-blocks', 'wp-element', 'wp-editor'],
            filemtime(plugin_dir_path(__FILE__) . 'taxdoo_toc/taxdoo_toc.js')
        );

        wp_register_style(
            'taxdoo_toc',
            plugins_url("taxdoo_toc/taxdoo_toc.css", __FILE__),
            [],
            filemtime(plugin_dir_path(__FILE__) . 'taxdoo_toc/taxdoo_toc.css')
        );

        register_block_type("taxdoo/taxdoo-toc", [
            'style' => 'taxdoo_toc',
            'editor_script' => 'taxdoo_toc',
            'render_callback' => [$plugin, 'block_toc_shortcode']
        ]);
    }

    public function taxdoo_excursus_block_init(){

        $plugin = new Plugin();



        wp_register_script(
            'taxdoo_excursus',
            plugins_url("taxdoo_excursus/taxdoo_excursus.js", __FILE__),
            ['wp-blocks', 'wp-element', 'wp-editor'],
            filemtime(plugin_dir_path(__FILE__) . 'taxdoo_excursus/taxdoo_excursus.js')
        );

        wp_register_style(
            'taxdoo_excursus',
            plugins_url("taxdoo_excursus/taxdoo_excursus.css", __FILE__),
            [],
            filemtime(plugin_dir_path(__FILE__) . 'taxdoo_excursus/taxdoo_excursus.css')
        );

        register_block_type("taxdoo/taxdoo-excursus", [
            'style' => 'taxdoo_excursus',
            'editor_script' => 'taxdoo_excursus',
           // 'render_callback' => [$plugin, 'testShortcode']
        ]);
    }
}
?>