( function( blocks, editor, element ) {
	var el = element.createElement;

	blocks.registerBlockType( 'taxdoo/taxdoo-excursus', {
		title: 'Taxdoo: Excursus', // The title of block in editor.
		icon: 'editor-table', // The icon of block in editor.
		category: 'common', // The category of block in editor.
        attributes: {
            content: {
                type: 'array',
                source: 'children',
                selector: 'p'
            },
            type: {type: 'string', default: 'default'},
            title: {type: 'string', selector: 'a'}
        },	
        edit: function(props){


            function updateTitle(event){
                props.setAttributes({title: event.target.value})
            }

            function updateContent(newdata){
                props.setAttributes({content: newdata})
            }

            function updateType(event){
                props.setAttributes({type: event.target.value})
            }


           return(
              el('div', {
                className: 'taxdoo_blog_content_box_backend taxdoo_blog_content_box_backend-' + props.attributes.type
              },
              el('label', {}, "Type:"),
              el(
                  'select',
                  {
                      onChange: updateType,
                      value: props.attributes.type
                  },
                  el("option", {value: "default"}, 'Default'),
                  el("option", {value: "success"}, 'success'),
                  el("option", {value: "danger"}, 'danger'),
              ),
              el('input',
                {
                    type: 'text',
                    placeholder: 'Enter title here...',
                    value: props.attributes.title,
                    onChange: updateTitle,
                    style: {width: '100%'}
                }
                ),
              el(
                  editor.RichText,
                  {
                      tagName: 'p',
                      onChange: updateContent,
                      value: props.attributes.content,
                      placeholder: "Enter text..."
                  }
              ))
              
           ) // end return
        }, // end edit	
        save: function( props ) {
            console.log(props);
            return (
                el('div',
            {
                className: 'taxdoo_blog_content_box taxdoo_blog_content_box-' + props.attributes.type
            }, 
            el(
                'span',
                {className: 'taxdoo_blog_content_box_paragraph'},
                props.attributes.title
            ),
            el(editor.RichText.Content, {
                tagName: 'p',
                value: props.attributes.content
            }))
            )
        },
	} );
} )( window.wp.blocks, window.wp.editor, window.wp.element );