( function( blocks, editor, element ) {
	var el = element.createElement;

	blocks.registerBlockType( 'taxdoo/taxdoo-toc', {
		title: 'Taxdoo: Table of Contents', // The title of block in editor.
		icon: 'editor-table', // The icon of block in editor.
		category: 'common', // The category of block in editor.
        attributes: {
            content: {
                type: 'html',
                default: 'Taxdoo | Table of Contents'
            },
        },		
        save: function( props ) {
            return (
                el( 'div', { className: props.className },
                    el( editor.RichText.Content, {
                        tagName: 'p',
                        className: 'taxdoo-toc-content',
                        value: props.attributes.content,
                    } ),
                )
            );
        },
	} );
} )( window.wp.blocks, window.wp.editor, window.wp.element );