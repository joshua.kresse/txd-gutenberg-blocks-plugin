jQuery(".blog__content, .entry-content").find("h1, h2, h3, h4, h5, h6").each(function(){
    var text = jQuery(this).text();
    var strippedText = jQuery(this).text().replace(/ /g,'_');
    var tagName = jQuery(this).prop("tagName");
    jQuery(this).attr("id", strippedText);

    var glossaryItem = "<a class='heading_"+ tagName.toLowerCase() +"' href=#"+strippedText+">"+text+"</a>"
    jQuery(".taxdoo_post_glossary").append(glossaryItem);
})


 // The function actually applying the offset
function offsetAnchor() {
    if (location.hash.length !== 0) {
      window.scrollTo(window.scrollX, window.scrollY - 100);
    }
  }
  
  // Captures click events of all <a> elements with href starting with #
  jQuery(document).on('click', 'a[href^="#"]', function(event) {
    // Click events are captured before hashchanges. Timeout
    // causes offsetAnchor to be called after the page jump.
    window.setTimeout(function() {
      offsetAnchor();
    }, 0);
  });
  
  // Set the offset when entering page with hash present in the url
  window.setTimeout(offsetAnchor, 0);