<?php

namespace TxdWp;
require_once "Blocks/Blocks.php";
use TxdWp\Blocks\Blocks;
class Plugin {

    public function __construct(){
        
        add_shortcode('foo', [$this, 'testShortcode']);
        $blocks = new Blocks();
        add_action( 'wp_enqueue_scripts', [$this, 'add_scripts']);

        add_action('init', [$blocks, 'taxdoo_toc_block_init']);
        add_action('init', [$blocks, 'taxdoo_excursus_block_init']);

    }

    public function testShortcode(){
        echo "FOOOOO";
    }


    public function add_scripts(){
        $my_plugin = WP_PLUGIN_URL . '/txd-gutenberg-blocks/src/includes/index.js';
        if(is_single() && 'post' == get_post_type()){
            wp_enqueue_script( 'taxdoo-glossary-js', $my_plugin , array('jquery'), '1.0.0', true );
        }

    }

    public function block_toc_shortcode(){
        ob_start();

        ?>
        
            <div class="taxdoo_post_glossary">
            <?php
                if(strpos($_SERVER['REQUEST_URI'], '/de/') !== false){
                    printf("<p><strong>Inhaltsverzeichnis</strong></p>");
                }
                else if(strpos($_SERVER['REQUEST_URI'], '/en/') !== false){
                    printf("<p><strong>Index</strong></p>");
                }
                else if(strpos($_SERVER['REQUEST_URI'], '/es/') !== false){
                    printf("<p><strong>Índice</strong></p>");
                }
                else if(strpos($_SERVER['REQUEST_URI'], '/it/') !== false){
                    printf("<p><strong>Indice</strong></p>");
                }
                else if(strpos($_SERVER['REQUEST_URI'], '/fr/') !== false){
                    printf("<p><strong>Sommaire</strong></p>");
                }
                ?>
            </div>
            <style>

                .taxdoo_post_glossary p {
                    font-style: oblique;
                }
                .taxdoo_post_glossary a {
                    display: block;
                    text-decoration: none;
                    margin: 5px 0;
                }
				
				.taxdoo_post_glossary {
					border: 2px solid black;
					padding: 20px;
					margin: 20px 0;
				}

                .taxdoo_post_glossary .heading_h2 {
                    display: list-item;
                    list-style-type: disc;
                    list-style-position: inside;
                    font-weight: 900;
                }

                .taxdoo_post_glossary .heading_h3 {
                    display: list-item;
                    list-style-type: disc;
                    list-style-position: inside;
                }

                .heading_h1 {
                    font-size: 20px;
                }
                .heading_h2 {
                    font-size: 18px;
                    padding-left: 10px
                }
                .heading_h3 {
                    font-size: 14px;
                    padding-left: 30px
                }
                .heading_h4 {
                    font-size: 14px;
                    padding-left: 30px
                }
                .heading_h5 {
                    font-size: 14px;
                    padding-left: 40px
                }
                .heading_h6 {
                    font-size: 14px;
                    padding-left: 50px
                }

                body {
                    overflow-anchor: none;
                }
            </style>
        <?php

      

       ?>
        


    <?php

        $content = ob_get_clean();
        return $content;

    }
}
?>