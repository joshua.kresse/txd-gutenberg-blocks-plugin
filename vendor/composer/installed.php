<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '5b987d88eab753eee2f3087d5b046278f4107f52',
        'name' => 'joshua.kresse/txd-gutenberg-blocks',
        'dev' => true,
    ),
    'versions' => array(
        'joshua.kresse/txd-gutenberg-blocks' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '5b987d88eab753eee2f3087d5b046278f4107f52',
            'dev_requirement' => false,
        ),
    ),
);
